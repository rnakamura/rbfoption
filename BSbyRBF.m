function [pr L c y t] = BSbyRBF(S,K,r,q,T,v,CP, N, M, b)
% Calculates price for European Option
% For call set CP = 1, for put CP = -1
% S = Spot share price
% K = Strike price
% r = Risk free interest rate
% q = Dividend yield
% T = Time to expiry
% v = Volatility
% b = Log-price boundary

%% RBF and derivatives
%f0 = @(x,y,c) sqrt((x-y).^2+c.^2);
%f1 = @(x,y,c) (x-y) ./ f0(x,y,c);
%f2 = @(x,y,c) 1 ./ f0(x,y,c) - (x-y).^2 ./ f0(x,y,c).^3;
rbfHardy
%rbfTPS
%eps = 0.0001; rbfHardyLukaszyk

%% Working variables
y = b*(2*(1:M)-M-1)/(M-1);
x = y';
dx = x(2)-x(1);
dt = T/N;
c = 4*dx;
m = length(x);

XA = repmat(x,1,length(x));
F0 = f0(XA,XA',c);
FI = inv(F0);
F1 = f1(XA,XA',c);
F2 = f2(XA,XA',c);
I = eye(size(XA));
L = nan(m,N+1);

%% Final condition
L(:,N+1) = FI * max(0,CP*(S*exp(x)-K));

%% Prepare matrix
dl = 1;
LD = 0.5*v^2*F2 + (r-q-0.5*v^2)*F1;
LD([1 m],:) = 0;
FD = F0;
FD([1 m],:) = 0;
FB = zeros(m,m);
FB([1 m],:) = F0([1 m],:);
h = zeros(m,1);
A = FD - dt*(FB+dl*LD);
B = FD + dt*(1-dl)*LD - r*dt*FD;
a = inv(A); % always same - economical to calculate invers

%% Solve interatively
for k = N:-1:1
  t = dt*(k-1);
  % Update boundary condition
  if CP > 0
    h(1) = 0; 
    h(m) = S*exp(x(m))-K*exp(-r*(T-t));
  else
    h(1) = K*exp(-r*(T-t)) - S*exp(x(1));
    h(m) = 0; 
  end
  % Take one step by delta-method (including boundary)
  %l = A \ (B*L(:,k+1) - dt*h); %this is slower
  l = a * (B*L(:,k+1) - dt*h); %this is faster 
  % Store for next iteration
  L(:,k) = l;
end

pr = f0(0,y,c)*l;

%g = @(x,y,c,l) arrayfun(@(x0) f0(x0,y,c) * l,x);

end
