function f0 = Hardy0(r,c) 
  f0 = sqrt(r.^2+c.^2);
end
