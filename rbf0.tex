\input{./useuse}

\DeclareMathOperator*{\myarg}{arg}

\usepackage[small,compact]{titlesec}
\usepackage{tikz,pgfplots}
\usepackage{caption}
%\documentclass[a4paper]{article}
\usepackage[affil-it]{authblk}

\begin{document}
%
\newcommand{\mycourse}{Technical note:}%
\newcommand{\myclass}{Radial Basis Functions for Pricing and Risk}% 
\newcommand{\mydirectory}{TN0}%
\newcommand{\myID}{ID1092387456}%
\newlength\figureheight%
\newlength\figurewidth%
\setlength\figureheight{6cm}%
\setlength\figurewidth{9cm}%
%
%\input{../headfoot}
%\setlength{\headheight}{40pt}
%\mytitle
%

\title{Radial Basis Functions for Pricing and Risk}
\author{Rickard Brannvall%
  \thanks{email: \texttt{rickard.brannvall@gmail.com}}} %; Corresponding author}}
%\affil{University College London}

\date{June 24, 2013 (v1.0)\\ \today \,(v1.1)}

\maketitle

Numerical approximation schemes based on Radial Basis Functions (RBF) have been demonstrated to give reliable price estimates for many financial derivatives. The basic approach for pricing derivatives by PDE collocation is first briefly reviewed in this article and applied in a simulation-revaluation Monte Carlo to calculate Value at Risk and Potential Future Exposure for a standard European option%{\it XXX also want to add a more exotic barrier option}
. It is demonstrated that used this way for option revaluation inside a risk Monte Carlo, the RBF method has comparable performance even to that of the closed form Black and Scholes pricing formula. As many exotic options can be treated with only minor modifications to the basic method, RBF-algorithms are proposed as a practical and computationally efficient alternative to standard techniques in quantitative risk management.    

Key-words: Radial Basis Functions, Option Pricing, Exotic Derivatives, VaR, PFE 

\section{Introduction}
\label{intro}

Banks and other financial institutions are required to measure the risk of their derivatives positions and set aside capital bufferts to meet regulatory requirements aimed at securing the health of the financial system. The counterparty credit risk contribution to capital is estimated by Monte Carlo simulation where the derivative contract must be priced for a large number of scenarios - these so called simulation-revaluation approach to risk estimation can be very computationally intensive, particularly for large portfolios of derivatives, and usually only applied for simple derivatives.

Although risk-sensitivities (greeks) are still often used for market risk estimation, this method may be insufficient for products with significant convexity or multiple factor dependence (cross-gamma), in which case market risk managers also will have to resort to the more expensive Monte Carlo simulation-revaluation techniques. Let's assume we have to revalue a single contract 10,000 times to estimate the risk and that a single price estimate takes 10 ms (many exotic derivatives take seconds to minutes to price) - a single contract would take more than 15 minutes and a large portfolio of trades could run into several days or even be completely untractable by this approach.   

Significant progress have been made in the last few years to also handle also exotic derivatives (see for example \cite{Cesari}) using finite difference schemes or least-square Monte Carlo techniques that give as output a grid of derivative prices. Although these methods are already part of advanced institutions repertoire to calculate counterparty credit exposure and credit value adjustment (CVA), some fundamental limitations as well as important implementation issues remain. PDE collocation of radial basis functions offer an attractive alternative, as these methods are known to work well also in high dimensions by avoiding the curse of dimensionality \cite{Uppsala}. This paper is a numerical study of the application of the RBF techniques to risk estimation for a simple option.

\section{Methodology}
\label{method}

We start with the Black and Scholes partial differential equation for the price of a derivative contract on a non-dividend paying stock 

$$ 
\frac{\partial V}{\partial t} + \frac{1}{2}\sigma^2 \frac{\partial^2 V}{\partial x^2} 
+ \left(r - \frac{1}{2}\sigma^2 \right)  \frac{\partial V}{\partial x} - rV = 0
$$

where $x=\log(S/S_0)$ is the logarithm of the stock price $S$ at time $t$ normalised to the starting price $S_0$ at $t_0 = 0$.  In operator form

$$
\frac{\partial V}{\partial t} + \mathcal{L} V - rV = 0
$$ 

In the following sections that describe the algorithm for pricing the derivative we follow Pe{\~n}a's introduction \cite{Pena} (with a few variations in the notation and implementation).

\subsection{Radial Basis Functions}

Radial basis functions have been demonstrated to give reliable numerical solutions for option pricing problems by finite difference \cite{HonMao}. An approximate solution $\hat{V}$ is written as a sum of basis functions over a set of $M$ nodes 

$$
\hat{V}(x,t_n) = \sum_{j=1}^{M} \phi (x-y^j) \, w^j_n
$$ 

Let the variable $x$ and the position of the nodes $\{ y^j \}$ be understood as log stock prices. The ordering of the collocation nodes is arbitrary, however, in this simple one-dimensional application we will require that $y^j  > y^{j-1}$.  Note that the superscript here indicates node index, not exponent. Time dependence enter only through the coefficients $w^j_n$ valid at time $t_n$. We choose Hardy's multi-quadric as radial basis function \cite{Hardy71}, as it has been demonstrated appropriate for the pricing problem we are addressing \cite{Uppsala}.  

$$
\phi (x-y) = \sqrt{ \left( x - y \right)^2 + c^2 }
$$

The function takes an additional {\it shape} parameter $c$ that depends on the application. The derivatives of  $\phi$ are used in the operator $\mathcal{L}$ and can be evaluated from

$$
\frac{\partial \phi}{\partial x} = \frac{x-y}{\phi}
$$

$$
\frac{\partial^2 \phi}{\partial x^2} = \frac{1}{\phi} - \frac{(x-y)^2}{\phi^3} 
$$

\subsection{Final and Boundary Conditions}

A backward finite difference scheme requires a final condition - here it is given by the payoff function $p$ for the option evaluated on the collocation nodes. At maturity the payoff for the call option with strike price $X$ is

$$
p_c(x^i) = \max \left( 0, S_0 \exp ( x^i ) - X \right)
$$

and for the put option with strike $X$

$$
p_p(x^i) = \max \left( 0, X - S_0 \exp ( x^i ) \right)
$$

For simplicity, we use a far-field Dirichlet boundary condition. Take $x^1$ (small) and $x^M$ (large) as the lower and upper boundaries and set for the call option 

\begin{align*}
h_c(x^1, t) & = 0 \\
h_c(x^M, t) & = S_0  \exp \left( x^M \right) - X \exp \left(-r \, [T-t] \right)
\end{align*}

and for the put option 

\begin{align*}
h_p(x^1,t) & = X \exp \left(-r \, [T-t] \right) - S_0 \exp \left( x^1 \right) \\
h_p(x^M,t) & = 0
\end{align*}

Generally we write the boundary conditions as $\hat{V}(x^b, t) = h(x^b, t)$, where $x^b$ is either of $x^1$ and $x^M$ and $h$ is either of $h_c$ or $h_p$. Similarly we take $x^d$ to mean all nodes $x^i$ strictly inside the domain bounded by $x^1$ and $x^M$.  In other words, we have that $b \in \{ 1, M \} $ and $d \in \{ 2, \dots, M-1 \} $. 

\subsection{Matrix Notation}

Here we will introduce some matrix notation to simplify the equations. Let $\varphi^{i,j} = \phi(x^i, y^j)$ be short hand for $\phi(x,y)$ at $x^i$ and $y^j$. Evaluate for all nodes $i$ and $j$ 

$$
\mathbf F =
  \begin{pmatrix}
  \varphi^{1,1} & \varphi^{1,2} & \cdots & \varphi^{1,M} \\
  \varphi^{2,1} & \varphi^{2,2} & \cdots & \varphi^{2,M} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  \varphi^{M-1,1} & \varphi^{M-1,2} & \cdots & \varphi^{M-1,M} \\
  \varphi^{M,1} & \varphi^{M,2} & \cdots & \varphi^{M,M}
 \end{pmatrix}
$$

This matrix can be split into two matrices according to rows pertaining to the boundary points $\{x^1, x^M\}$ and to points strictly in the domain $x^d$ for $1<d<M$ respectively

$$
\mathbf F^B =
  \begin{pmatrix}
  \varphi^{1,1} & \varphi^{1,2} & \cdots & \varphi^{1,M} \\
  0 & 0 & \cdots & 0 \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  0 & 0 & \cdots & 0 \\
  \varphi^{M,1} & \varphi^{M,2} & \cdots & \varphi^{M,M}
 \end{pmatrix}
$$


$$
\mathbf F^D =
  \begin{pmatrix}
  0 & 0 & \cdots & 0 \\
  \varphi^{2,1} & \varphi^{2,2} & \cdots & \varphi^{2,M} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  \varphi^{M-1,1} & \varphi^{M-1,2} & \cdots & \varphi^{M-1,M} \\
  0 & 0 & \cdots & 0
 \end{pmatrix}
$$

Finally, we define $\mathbf L^D$ to be the operator $\mathcal{L}$ applied to matrix $\mathbf F^D$

$$
\mathbf L^D =
  \begin{pmatrix}
  0 & 0 & \cdots & 0 \\
  \mathcal{L}\varphi^{2,1} & \mathcal{L}\varphi^{2,2} & \cdots & \mathcal{L}\varphi^{2,M} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  \mathcal{L}\varphi^{M-1,1} & \mathcal{L}\varphi^{M-1,2} & \cdots & \mathcal{L}\varphi^{M-1,M} \\
  0 & 0 & \cdots & 0
 \end{pmatrix}
$$

\subsection{Pricing Algorithm}

We are now ready to write the numerical algorithm in matrix form. The final condition give us the following equation for the weights at maturity $t_N = T$

$$
\mathbf{F} \, \mathbf w_{N} = \mathbf{p}
$$

where we have written both the weights $\mathbf{w_N}$ and the final payoff $\mathbf{p}= p(\mathbf{x})$ as column vectors. As we know both $\mathbf{F}$ and the option payoff we can solve for $\mathbf w_{N}$. 

Inside the domain we write the differential equation in matrix form

$$
\frac{\partial V}{\partial t} +  \mathcal{L} V= r V
\quad \Rightarrow \quad
\mathbf F^D \, \frac{\partial \mathbf{w}}{\partial t} + \mathbf L^D  \, \mathbf{w} = r \,  \mathbf F^D \, \mathbf{w} 
$$

Note how time-differentiation only applies to the weights, as $\mathbf F$ has no time-dependence by itself. For the boundary conditions we similarly have 

$$
\hat{V}(x^b, t) = h(x^b, t)
\quad \Rightarrow \quad
\mathbf F^B \, \mathbf{w} = \mathbf h^B
$$


where $\mathbf h^B$ is a vector with zero in every row corresponding to a node strictly in the domain, while the first and last row are given by the boundary values. Next we write the matrix equations jointly and in finite difference form over the discrete time step $\Delta t = T / N$

$$
\mathbf F^D \, \frac{\mathbf w_{n+1} - \mathbf w_{n}  }{\Delta t} + \mathbf L^D \, \mathbf w_{n}  + \mathbf F^B \, \mathbf w_{n} = r \,  \mathbf F^D \, \mathbf w_{n+1}  + \mathbf h^B_{n}
$$

%fix superscript of w and h

after collecting terms belonging to the weight vector at each time-point, we get

$$
\mathbf A \, \mathbf w_{n} = \mathbf B \,  \, \mathbf w_{n+1}  -  \Delta t \,  \mathbf h^B_{n}
$$

with new matrices $\mathbf A$ and $\mathbf B$ given by

$$
 \mathbf A = \mathbf F^D  - \Delta t \, (  \mathbf L^D + \mathbf F^B )
$$

$$
 \mathbf B = \mathbf F^D  - \Delta t \; r \; \mathbf F^D
$$

This is the core equation that we solve at each time step. For equal time-step $\Delta t$ and time-homogenous $\mathbf L^D$ and $r$, it may be efficient to pre-calculate $\mathbf B$ and the inverse of $\mathbf A$ (but $\mathbf h^B_{n}$ has time-dependence and has to be recalculated at each step). After iterating for all $n<N$ until $n=0$, we arrive at the approximate price at $t=0$ 

$$
\hat{V}(x, 0) = \sum_{j=1}^{M} \phi (x-y^j) \, w^j_0
$$

We can now use $x=\log(S/S_0)$ to transform between dollar prices and normalised log prices. Note that this formula estimates the price for any $x$ between $x^1$ and $x^M$, not only on nodes. 

In the following we will refer to the algorithm just described as the RBF-algorithm.
%Similarly, it can also be used with $\mathbf w_n$ to revaluate for a future time $t_n$ and potential log stock price $x$.

\subsection{VaR and PFE}
\label{VaRandPFE}

If we use $\mathbf w_{n}$ in place of $\mathbf w_{0}$ we can also price contracts for future time $t_n$ and potential stock price $S_n$. This allow us to efficiently calculate risk estimates such as Value at Risk (VaR) and Potential Future Exposure (PFE) by Monte Carlo methods, without having to re-run the numerical pricing algorithm at every revaluation point.

We estimate VaR by generating potential stock prices at $t = 10$ days according to a simple log-normal model of the stock price (see for example McNeil et al \cite{McNeil})

$$
S_t = S_0 \exp \left( \varepsilon \, \sigma \, \sqrt{t} + (r - 0.5 \, \sigma^2) \, t \right)
$$

where $\varepsilon$ is a standard normal random draw, $t$ is time expressed as a fraction of a year and $\sigma$ and $r$ annualised volatility and risk-free interest rate respectively. Note that we let the drift rate in the equation be the risk-free rate, hence we estimate risk metrics under the risk neutral measure. The option price at $t=10$ days is calculated using the RBF approximation. The change in option price over the ten day period is given by

$$
\Delta p = p(S_t, t) - p(S_0, 0) 
$$

From a sample of $K$ simulated potential stock prices, we can extract the k-th largest loss (i.e. the k-th most negative change in option price) to estimate the 10 day VaR at confidence level $\alpha = k / K$. $K$ is typically 1000 or larger for most applications.

Similarly, we can estimate PFE by simulating log-normal potential stock prices for a series of future time points up until expiry of the contract. The reader is referred to Gregory \cite{Gregory} or Pykhtin \cite{Pykhtin} for more detailed examples of the steps in exposure estimation. In the case of PFE we focus on the potential future price $p(S(t), t)$ for each $t=t_n$, and define PFE(t) as the k-th largest potential price at time $t$.

\section{Results}
\label{result}

In this section the algorithms for pricing and estimating risk are typically tested for a one year at-the-money (ATM) equity option, i.e. an option for which the strike price equals the current spot price. The numerical values for each model parameter is listed in Table \ref{tab:Params}, both for simulation in the log-normal model and pricing by the Black \& Scholes (B\&S) formula and the RBF-algorithm. Unless otherwise specified we use these

\begin{table}[!ht]
\centering
\begin{tabular}{c c l}
Parameter& Value & Description \\
\hline
$S_0$	& 100 & Initial Stock Price \\
$X$	& 100 & Option Strike Price \\
$\sigma$	& 0.30 & Annual Volatility \\
$r$	& 0.01 & Risk Free Rate \\
$T$	& 1.0 & Option Expiration (Years) \\
\hline
$N$	& 52 & Number of Time-steps  \\
$M$	& 60 & Number of RBF nodes \\
$x^1$	& -2 & Lower Boundary \\
$x^M$	& 2 & Upper Boundary \\
\end{tabular}
\caption{%
%\begin{footnotesize}%
Model parameters and their typical numerical values.%
%\end{footnotesize}%
}%
\label{tab:Params}
\end{table}

The pricing algorithm is typically run over $N=52$ time steps with $M=60$ radial basis functions located at equidistant nodes between the boundaries. These parameters will be varied and examined in the error and stability analysis that follows. The shape parameter $c$ for Hardy's multiquadric RBF is taken at the standard value \cite{Hardy90}:

$$ c = 4 \, \Delta x$$

where $\Delta x$ is the distance (in log price) between two adjacent collocation nodes.

\subsection{Price for European Option}

First we price a series of options with different strike; results are displayed in Table \ref{tab:Price}, where we see that the ATM option (1.0x) is priced correctly down to the cent with our standard numerical set-up ($N=52$, $M=60$) compared to the B\&S formula. The other contracts in the table also show only a difference of a few cents (typically within the bid-ask spread that exist in the market).

\begin{table}[!ht]%
\centering%
\input{tblPrice.tex}%
\caption{%
%\begin{footnotesize}%
Estimated price in dollars for a European option with notional \$100 for a range of strikes as multiples of spot price.%
%\end{footnotesize}%
}
\label{tab:Price}
\end{table}

As can be expected for any numerical pricing algorithm, the error depends on the size and granularity of the "grid". Table \ref{tab:ErrorRBF} list the error defined as the dollar difference between the RBF price and the B\&S formula price. It is clear that for single cent precision $M$ should lie between 40 and 100, with $N$ around 20 to 100. Too small a grid suffers from lack of precision, while a too large grid have problem with stability. This is further examined in Figure \ref{fig:MErrorRBF} and \ref{fig:NErrorRBF} where we can see evidence of the balance between precision and stability. Here we have used simple ones-step time-differentiation, that is only first order accurate - a more advanced scheme may improve stability as demonstrated in e.g. Pettersson et al \cite{Uppsala}. We deemed however that level of accuracy not necessary for our application of the method to risk.

\begin{table}[!ht]%
\centering%
\input{tblErrorRBF.tex}%
\caption{%
%\begin{footnotesize}%
Estimation error in dollars for a European call option with notional \$100 (price by B\&S formula is \$12.37). Number of RBF nodes $M$ and time-steps $N$ is varied.%
%\end{footnotesize}%
}
\label{tab:ErrorRBF}
\end{table}

\subsection{Risk for European Option}

%
\begin{figure}%[!ht]
\centering%
\input{figPFEofCall.tex}%
\caption{%
%\begin{footnotesize}%
PFE profile at 97.5\% confidence estimated by Monte Carlo simulation using both the RBF approximation and B\&S formula for revaluation.%
%\end{footnotesize}%
}%
\label{fig:PFEofCall}
\end{figure}%
%

Potential Future Exposure (PFE) and Value at Risk (VaR) are estimated (as described in Section \ref{VaRandPFE}) by using a Monte Carlo simulation to price the options over $K$ potential future stock-price paths: typically $K=1000$ and for convenience we set the time-step the same as in the RBF pricing, i.e. $N=52$. With this time-step, ten day corresponds to the second time-point: $n=2$; three months to $n=13$ and six months to $n=26$. Revaluation is done both by B\&S formula and the RBF algorithm, with risk estimates displayed in Table \ref{tab:Risk} for VaR and PFE for a few time buckets. 

\begin{table}[!ht]%
\centering%
\input{tblRisk.tex}%
\caption{%
%\begin{footnotesize}%
VaR and PFE estimated by Monte Carlo with 1000 paths (52 time-steps for PFE). Revaluation by either RBF or B\&S. PFE at $97.5\%$ and VaR at $99\%$ confidence.%
%\end{footnotesize}%
}
\label{tab:Risk}
\end{table}

The Monte Carlo simulation was carried out using the same random sequence, hence the differences are only due to the pricing method: we can see that, to the precision we measure risk (dollars on a \$100 notional), the differences are immaterial (a few cents). Figure \ref{fig:PFEofCall} shows the whole PFE profile for the one year call option: note how estimates using RBF and B\&S are close enough to be indistinguishable in the figure.

\begin{table}[!ht]%
\centering%
\input{tblTime.tex}%
\caption{%
%\begin{footnotesize}%
Execution time$^1$ - First call to RBF is expensive (\textit{price once}), subsequent pricing much cheaper (\textit{price again}). Risk Monte Carlo: 1000 paths (x 52 steps for PFE).%
%\end{footnotesize}%
}
\label{tab:Time}
\end{table}

Execution times on a modern dual-core laptop\footnote{on a 2.13 GHz Intel core 2 Duo running Mac OS 10.8}  are displayed in Table \ref{tab:Time}. While the initial call to the RBF algorithm is time-consuming (10.5 ms) as it must iterate over the finite difference and collocation step, subsequent calls make use of the stored array of weights $\{ \mathbf w_{0}, \mathbf w_{1}, \dots, \mathbf w_{N} \}$ to reduce pricing to a single vector product (0.2 ms). 

$$
f(x, t_n) = \sum_{j=1}^{M} \phi (x-y^j) \, w_j^n
$$

The B\&S formula always takes the same time and involves calls to several mathematical functions: exp, log and the normal cdf (cumulative distribution function) - those should however be efficiently implemented in most programming languages (and certainly in Matlab/Octave that we used here for simulations). The execution times will of course depend on the implementation and machine performance  - we can mention here that our implementation is fully vectorized over the path dimension both for RBF pricing (\textit{price again}) and in the B\&S formula (i.e.\;calculates bunches of $K$ prices in one function call to avoid overhead). The RBF numerical scheme (\textit{price once}) uses matrix operators where appropriate and only contain a single for-loop (i.e.\;over time).

The first pricing instance of the RBF is much slower than the B\&S formula (although remember that the numbers in Table \ref{tab:Time} include interpreter overhead - compiled code should show larger difference for the single call). However, for the risk estimations the execution times compare, with VaR taking only three times longer using RBF (14.3 ms vs 5.7 ms) and the ratio even slightly reversed for the 52 time-step PFE (183.1 ms vs 203.4 ms). This is further illustrated in Figures \ref{fig:ExecTime} and \ref{fig:ExecGain} where we can see the RBF approach overtaking the formula already at around 20,000 pricing instances. This is a computational gain even for a vanilla option (that have analytical pricing formula) at Monte Carlo simulation sizes that are realistic for counterparty risk estimation. 

Many exotic derivatives, having non-standard barriers or other features such as target redemption or Bermudan calls could be priced in this scheme with only minor modifications. Performance would still be comparable to the simple option, which could make RBF methods an attractive alternative to Finite Difference and American Monte Carlo methods that have been proposed for risk estimation of exotic derivatives \cite{Cesari}.

\section{Conclusions}
RBF pricing was demonstrated to be efficient for estimating risk of a simple vanilla option at simulation times not very different that which uses an analytical pricing formula. A more exotic option could be handled by the same numerical scheme with little additional computational cost. This is perhaps where the application of RBF methods to risk is most promising: an additional tool that complement regular Finite Difference schemes and American Monte Carlo techniques for computer intensive applications in risk. 

%Note: An online calculator is available at nakamuraseminars.com\footnote{www.nakamuraseminars.com/community/rbfsimple} to exemplify the price and risk estimations in this article. There you can also download the code. 

\begin{thebibliography}{1}
\bibliographystyle{plain}

\bibitem{Hardy71} Hardy RL, "Multiquadric equations of topography and and other irregular surfaces", J of Geophysical Res (1971), 76:1905-15.
\bibitem{Hardy90} Hardy RL, "Theory and applications of the multiquadric-biharmonic method: 20 years of discovery", Comput Math Applic 19 (8/9), 163-208, (1990).
\bibitem{Hon02} Hon Y. C., "A quasi-radial basis functions method for American options pricing", Comput. Math. Appl., 43 (2002), pp 513�524.
\bibitem{HonMao} Hon Y. C., Mao X. Z., "A radial basis function method for solving options pricing model", J. Financial Engineering, 8 (1999), pp.1�24.
\bibitem{Pena} Pe{\~n}a A, "Option Pricing with Radial Basis Functions: A Tutorial", Wilmott Magazine (2010-11).
%\bibitem{Uppsala} Pettersson U , Larsson E, Marcusson G, and Persson J, "Option Pricing using Radial Basis Functions",, on-line manuscript (2005).
\bibitem{Uppsala} Pettersson U , Larsson E, Marcusson G, and Persson J, "Improved radial basis function methods for multi-dimensional option pricing", Journal of Computational and Applied Mathematics, 222 (2008), pp. 82-93.
\bibitem{Pykhtin} "A Guide to modelling Counterparty Credit Risk", Michael Pykthin and Steven Zhu, GARP Risk Review, July/August 2007 (Issue 37).
\bibitem{Gregory} "Counterparty Credit Risk", Jon Gregory, XXX (XXXX).
\bibitem{Cesari} Cesari et al, "Modelling, Pricing, and Hedging Counterparty Credit Exposure: A Technical Guide", (Springer Finance 2010)
\bibitem{McNeil} McNeil  A, Frey R, Embrechts P "Quantitative Risk Management: Concepts Techniques and Tools.", Princeton University Press (2005).
\end{thebibliography}


~

%
\begin{figure}[!ht]%
\centering%
\input{figMErrorRBF.tex}%
\caption{%
%\begin{footnotesize}%
Error and stability analysis with increasing number of nodes M (for three different N). Note the balance between error (small M) and instability (large M).%
%\end{footnotesize}%
}%
\label{fig:MErrorRBF}
\end{figure}%
%

%
\begin{figure}[!ht]%
\centering%
\input{figNErrorRBF.tex}%
\caption{%
%\begin{footnotesize}%
Error and stability with increasing number of time-steps N (for four different M). Note the balance between error (small N) and instability (large N).%
%\end{footnotesize}%
}%
\label{fig:NErrorRBF}
\end{figure}%
%

%
\begin{figure}[!ht]%
\centering%
\input{figExecTime.tex}%
\caption{%
%\begin{footnotesize}%
Execution time (in bundles of 1000) of RBF revaluation vs B\&S formula.%
%\end{footnotesize}%
}%
\label{fig:ExecTime}
\end{figure}%
%

%
\begin{figure}[!ht]%
\centering%
\input{figExecGain.tex}%
\caption{%
%\begin{footnotesize}%
Ratio of execution time: RBF approach over B\&S formula.%
%\end{footnotesize}%
}%
\label{fig:ExecGain}
\end{figure}%
%


%\end{twocolumn}
\end{document}  
