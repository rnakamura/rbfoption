%vardef
output1 = outputok;
clear tbl tmr;
for k = 0:1
 VaRbyRBF
 CP = (-1)^k;
 tmr(1+k*2,1:3) = [t0 ti t1];
 tmr(2+k*2,1:3) = [t3 t3 t2];
 tbl(1+k*2,1) = rbfvar;
 tbl(2+k*2,1) = bsfvar;
 outputok = false; 
 PFEbyRBF
 CP = (-1)^k;
 tmr(1+k*2,4) = t1;
 tmr(2+k*2,4) = t2;
 for i=1:4
  tbl(1+k*2,i+1) = rbfpfe(N*i/4);
  tbl(2+k*2,i+1) = bsfpfe(N*i/4);
 end
end
outputok = output1;

if outputok
cl = {'10d VaR' '3m PFE', '6m PFE', '9m PFE', '12m PFE'};
rl = {'Call by RBF' 'Call by B\&S', 'Put by RBF', 'Put by B\&S'};
matrix2latex(tbl, 'tblRisk.tex','mode','N','rowLabels', rl, 'alignment','c','format','%0.2f', 'columnLabels', cl)
end
