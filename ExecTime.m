vardef
NTOT = 10;
outputok = true;
NN = 100;
tot = zeros(2,NN);
x = randn(K,1)/10;
s = S*exp(x);
f = @(x,y,c) sqrt(bsxfun(@minus, x, y).^2+c.^2);
g = @(x,y,c,l) f(x,y,c)*l;

for i=1:(NTOT+1)
  tmr = zeros(2,NN);
  % by RBF
  tic;
  [pr L c y] = BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
  for k = 1:NN
    g(x,y,c,L(:,1));
    tmr(1,k) = toc;
  end
  % by B&S
  tic;
  for k = 1:NN
    BS(s,X,r,v,T,CP);      
    tmr(2,k) = toc;
  end
  if i>1
    tot = tot+tmr;
  end
end
KK = K*(1:NN);
tot = tot/NTOT*1000;

if outputok
myplot(KK, {tot(1,:) tot(2,:)}, {'RBF', 'B&S'}, 'no executions', 'execution time(ms)', [])
matlab2tikz( 'figExecTime.tex', 'height', '\figureheight', 'width', '\figurewidth' );
myplot(KK, tot(1,:)./tot(2,:), [], 'no executions', 'Ratio: RBF / B\&S', []) 
matlab2tikz( 'figExecGain.tex', 'height', '\figureheight', 'width', '\figurewidth' );
end
