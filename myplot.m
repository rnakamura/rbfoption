function myplot(X, Y, L, xl, tl, ax)
  % Format:
  % myplot(X, Y, L, xl, tl, ax)
  % Examples:
  % myplot(0:52, PFE1, 'PFE', 'week', 'Exposure', [0 60 0 75])
  % myplot({0:52, 0:156}, {PFE1, PFE3}, {'PFE1', 'PFE3'}, 'week', [], [])
  % myplot(0:52, {PFE1 PFE3(1:53)}, [], [], 'Two Exposures', [])

  figure;
  hold;
  cl = {'r', 'k--', 'r-.', 'k:'};

  if ~iscell(X)
   X = {X};
  end
  if ~iscell(Y)
   Y = {Y};
  end

  for i=1:size(Y,2)
    j = min(size(X,2),i);
    plot(X{j},Y{i}, cl{i});
  end

  if length(xl) > 0
    xlabel(xl);
  end
  if length(L) > 0
    legend(L, 'location', 'NORTHWEST')
  end
  if length(ax) > 0
    axis(ax);
  end
  if length(tl) > 0
   title(tl)
  end
end
