vardef
tbl = nan(4,5); 
for k = 0:1
 CP = (-1)^k;
 for i = 1:5
  mu = 1+(i-3)/10;
  cl{i} = sprintf('%0.1fx', mu); 
  pr = BSbyRBF(S,X*mu,r,q,T,v,CP, N, M, b);
  bs = BS(S,X*mu,r,v,T,CP);
  tbl(1+k*2,i) = pr;
  tbl(2+k*2,i) = bs;
 end
end

if outputok %true
%cl = {'10d VaR' '3m PFE', '6m PFE', '9m PFE', '12m PFE'};
rl = {'Call by RBF' 'Call by B\&S', 'Put by RBF', 'Put by B\&S'};
matrix2latex(tbl, 'tblPrice.tex','mode','N','rowLabels', rl, 'alignment','c','format','%0.2f', 'columnLabels', cl)
end
