% RBF and derivatives
f0 = @(x,y,c) Hardy0(x-y,c);
f1 = @(x,y,c) Hardy1(x-y,c);
f2 = @(x,y,c) Hardy2(x-y,c);
