function p = BS(S,K,r,s,T,CP)
 if T>0
  d1 = log(S./K)+(r+0.5*s.^2)*T;
  d1 = d1 ./ s ./ sqrt(T);
  d2 = d1 - s .* sqrt(T);
  p = S.*normcdf(d1*CP)-K.*exp(-r*T).*normcdf(d2*CP);
  p = p * CP;
 else
  p = (T==0)*max(0, CP*(S-K));
 end
end 
