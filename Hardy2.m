function f2 = Hardy2(r,c)
  f0 = sqrt(r.^2+c.^2);
  f2 = 1 ./ f0 - r.^2 ./ f0.^3;
end
