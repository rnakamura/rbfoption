vardef
output2 = outputok;
tot = zeros(4);
for i=1:NTOT
  outputok = false;
  RiskByRBF;
  tot = tot + tmr;
end
tot = tot / NTOT * 1000;
outputok = output2;

if outputok
cl = {'Price once' 'Price again', 'VaR MC', 'PFE MC'};
rl = {'Call by RBF' 'Call by B\&S', 'Put by RBF', 'Put by B\&S'};
matrix2latex(tot, 'tblTime.tex','mode','N','rowLabels', rl, 'alignment','c','format','%0.1f ms', 'columnLabels', cl)
end
