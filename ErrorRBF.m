vardef
NN = [4 12 26 52 100 200 300 500];
MM = 20:20:120;
clear pr
for n = 1:length(NN)
  N = NN(n);
  rl{n} = num2str(N);
  for m = 1:length(MM)
    M = MM(m);
    cl{m} = num2str(M);
    pr(n,m) = BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
  end
end
bs = BS(S,X,r,v,T,CP);
err = pr - bs;

if outputok
cl{1} = ['M = ' cl{1}]; %cl = {'1' '2', '3', '4', '5', '6'};
rl{1} = ['N = ' rl{1}]; %rl = {'1' '2', '3', '4', '5', '6', '7', '8'};
matrix2latex(err, 'tblErrorRBF.tex','mode','N','rowLabels', rl, 'alignment','c','format','%0.3f', 'columnLabels', cl)
end
