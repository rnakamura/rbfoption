vardef
clear pr;
NN = [50 100 150];
MM = 20:1:100;
for n = 1:length(NN)
  N = NN(n);
  rl{n} = num2str(N);
  for m = 1:length(MM)
    M = MM(m);
    cl{m} = num2str(M);
    pr(n,m) = BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
  end
end
bs = BS(S,X,r,v,T,CP);
err = pr - bs;

if outputok
cl{1} = ['M = ' cl{1}]; %cl = {'1' '2', '3', '4', '5', '6'};
rl{1} = ['N = ' rl{1}]; %rl = {'1' '2', '3', '4', '5', '6', '7', '8'};
myplot(MM, {err(1,:) err(2,:) err(3,:)}, rl, 'M', 'Error (dollars)', []) 
matlab2tikz('figMErrorRBF.tex', 'height', '\figureheight', 'width', '\figurewidth' );
end
