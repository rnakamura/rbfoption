vardef
BS(S,X,r,v,T,CP);
BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
%% Price Option by RBF
dt = 1/N;
t = zeros(1,N+1);
rbfpfe = zeros(1,N+1);
bsfpfe = zeros(1,N+1);
rn = randn(K,N);

tic;
[pr L c y] = BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
f = @(x,y,c) sqrt(bsxfun(@minus, x, y).^2+c.^2);
g = @(x,y,c,l) f(x,y,c)*l;

%% Estimate PFE by RBF
rbfpfe(1) = pr;
x = zeros(K,1);
for n=1:N
  t(n+1) = (n+1-1)/N;
  x = x + rn(:,n)*sqrt(dt)*v+(r-q-0.5*v^2)*dt;
  p = g(x,y,c,L(:,n+1));      	   % by RBF price approximation
  rbfpfe(n+1) = prctile(p,97.5);
end
t1 = toc;

tic;
%% Estimate PFE by B&S
bsfpfe(1) = BS(S,X,r,v,T,CP); 
x = zeros(K,1);
for n=1:N
  t(n+1) = (n+1-1)/N;
  x = x + rn(:,n)*sqrt(dt)*v+(r-q-0.5*v^2)*dt;
  p = BS(S*exp(x),X,r,v,T-t(n+1),CP);   % by BS formula
  bsfpfe(n+1) = prctile(p,97.5);
end
t2 = toc;

if outputok
myplot(t, {rbfpfe, bsfpfe}, {'RBF', 'B&S'}, 'time', 'PFE', [])
matlab2tikz( 'figPFEofCall.tex', 'height', '\figureheight', 'width', '\figurewidth' );
end
