function f1 = Hardy1(r,c)
  f1 = r ./ sqrt(r.^2+c.^2);
end
