vardef
BS(S,X,r,v,T,CP);
BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
%% Price Option by RBF
tic;
[pr L c y] = BSbyRBF(S,X,r,q,T,v,CP, N, M, b);
f = @(x,y,c) sqrt(bsxfun(@minus, x, y).^2+c.^2);
g = @(x,y,c,l) f(x,y,c)*l;
t0 = toc;

%% Estimate 10 day VaR by RBF
p0 = g(0,y,c,L(:,1));
ti = toc - t0;
n = 3; 	  % approx 10 days for N = 50, T = 1 
t = (n-1)/N;
x = randn(K,1)*sqrt(t)*v+(r-q-0.5*v^2)*t;
p = g(x,y,c,L(:,n));
rbfvar = -(prctile(p,1)-p0); %VaR is defined as the loss
t1 = toc;

%% Same estimate by BS formula
tic;
p0 = BS(S,X,r,v,T,CP);
t3 = toc;
p = BS(S*exp(x),X,r,v,T-t,CP);
bsfvar = -(prctile(p,1)-p0);
t2 = toc;
